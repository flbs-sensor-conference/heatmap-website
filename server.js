/**************************************************************************
 "Database" stuff
**************************************************************************/

/* 
	You'd want to use a real database in a production environment 
	(something like influxdb) to store the data, but using an array 
	will work fine as a quick demo.
*/

var json_sensor_data = require('./sensor_data.json');

function updateDataPoint(devID, board_temperature) {
	for (i in json_sensor_data.sensors) {
		if (json_sensor_data.sensors[i].name == devID) {
			json_sensor_data.sensors[i].board_temperature = board_temperature;
			json_sensor_data.sensors[i].timestamp = Date.now();
			return;
		}
	}
}

/**************************************************************************
 The Things Network stuff
**************************************************************************/

var ttn = require("ttn")

var appID = "sensor-conference"
var accessKey = "ttn-account-v2.Jkgm1aT6KmLIQwrfyQefree0g2yLGH0QyzLhCyF2_Ab"

ttn.data(appID, accessKey)
	.then(function (client) {
		client.on("uplink", function (devID, payload) {
			if ("payload_fields" in payload) {
				if ("board_temperature" in payload.payload_fields) {
					updateDataPoint(devID, payload.payload_fields.board_temperature.toFixed(1));
					console.log("\nReceived uplink from", devID);
					console.log("Temperature:", payload.payload_fields.board_temperature.toFixed(1));
				}
			}
		})
	})
	.catch(function (error) {
		console.error("Error", error)
		process.exit(1)
	})

/**************************************************************************
 Web server stuff
**************************************************************************/

var express = require("express");
var app = express();

app.use(express.static('public'));
app.use('/css', express.static(__dirname + '/public/css'));
app.use('/js', express.static(__dirname + '/public/js'));
app.use('/images', express.static(__dirname + '/public/images'));

app.get('/sensor_data', function (req, res) {
	res.send(json_sensor_data)
})

app.set('json spaces', 2);

var server = app.listen(8081, function () {
	var port = server.address().port;
	console.log("Server started at http://localhost:%s", port);
});