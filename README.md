### Instructions for installing and running the code

1. Install git 
https://git-scm.com/downloads

2. Install nodejs 
https://nodejs.org/en/download/

3. Right click on your desktop and click **Git Bash Here**. A terminal window will appear -- enter the commands below in the terminal window.

------------

Clone this project onto your desktop
1. `git clone https://gitlab.com/flbs-sensor-conference/heatmap-website.git`

Change directory into the project folder you just cloned

2. `cd heatmap-website`

Install the dependencies needed for this project

3. `npm install`

Run it!

4. `npm start`

Your local computer is now running a webserver, which is serving a website. Open the link below in your web browser to check it out...**but wait!**

**The program will crash** because you need to edit the **server.js** file in the project folder and add your own Things Network credentials. See the info below...

http://localhost:8081/


------------

### Editing the code

You'll need to edit the code so that it works with your own **Things Network** account.

1. Sign up for a Things Network account. It's free.
https://www.thethingsnetwork.org/

2. Add a new application. At the bottom make sure to select "ttn-handler-us-west" in the **Handler registration** section
https://console.thethingsnetwork.org/applications

3. Find your **Application ID** and  **Access Key** and get ready to copy them into the **server.js**  file

4. Open the **server.js** file in the heatmap-website folder that you cloned onto your desktop. Find the two lines shown below and replace them with your own Application ID and access key. Save your changes.

[![](https://i.imgur.com/YT7ADmo.png)]()

5.  Open the git bash terminal window. If your webserver is still running, press ctrl+c to close it. Start it again so that it uses your new changes by typing: `npm start`

------------

### Simulating sensor data

1. We need to add a **decoder** function in our Things Network application so that we can decode floating point values coming from our sensors into strings of text.

	Open the application you created a minute ago: https://console.thethingsnetwork.org/applications
	
	Click on the **Payload Formats** button .
	
	Select **Custom** for your payload format from the dropdown.
	
	Select the **decoder** button
	
	Paste this code into the field below the decoder button
	
	```
    function Decoder(bytes, port) {

    // Based on https://stackoverflow.com/a/37471538 by Ilya Bursov
    function bytesToFloat(bytes) {
        var bits = bytes[3]<<24 | bytes[2]<<16 | bytes[1]<<8 | bytes[3];
        var sign = (bits>>>31 == 0) ? 1.0 : -1.0;
        var e = bits>>>23 & 0xff;
        var m = (e == 0) ? (bits & 0x7fffff)<<1 : (bits & 0x7fffff) | 0x800000;
        var f = sign * m * Math.pow(2, e - 150);
        return f;
    }  

    // Test with 0082d241 for 26.3135 
    return {
        // Take bytes 0 to 4 (not including), and convert to float:
        board_temperature: bytesToFloat(bytes.slice(0, 4)),
        battery_voltage: bytesToFloat(bytes.slice(4, 8))
    };
    }
	```
	Save the changes by clicking the button at the bottom right.

2. Register a new device in your application. Enter "**board_1**" as the **Device ID** and have The Things Network automatically generate the Device EUI.

3. Click on your device and scroll down to the **Simulate Uplink** section. Paste in the following data and click send: `9D 73 CE 41 00 00 F7 42`

[![](https://i.imgur.com/Dkl3egd.png)]()


You should see something like this, showing that you just received data from a sensor.

[![](https://i.imgur.com/Q2w7wYG.png)]()

You should also see that the heatmap on your website changed slightly with the addition  of this new temperature value. Remember to open this link in your web browser to check out the heatmap:

http://localhost:8081/









