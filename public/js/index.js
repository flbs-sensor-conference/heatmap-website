/**************************************************************************
 Draws the heatmap when the page loads and updates it every 10 seconds
**************************************************************************/

$(document).ready(function () {
	updateDataAndHeatMap();
});

setInterval(function() {
    updateDataAndHeatMap();
}, 10 * 1000);

/**************************************************************************
 For retrieving the most recent sensor data and normalizing it for plotting
**************************************************************************/

var point_data = [];
var json_sensor_data_old;

function normalize(val, max, min) {
	return (val - min) / (max - min);
}

function updateDataAndHeatMap() {
	$.get("/sensor_data", function (data) {
		var json_sensor_data = data;
		
		// Check if any of the sensor data has changed, if not, exit function
		// because nothing needs to be done
		if (_.isEqual(json_sensor_data, json_sensor_data_old))
			return;
		
		json_sensor_data_old = json_sensor_data;

		// Find max and min temperature values (will use them to normalize the data between 0 - 1)
		var max_temp = json_sensor_data.sensors[0].board_temperature;
		var min_temp = json_sensor_data.sensors[0].board_temperature;
		for (i in json_sensor_data.sensors) {
			if (json_sensor_data.sensors[i].board_temperature > max_temp) {
				max_temp = json_sensor_data.sensors[i].board_temperature;
			}

			if (json_sensor_data.sensors[i].board_temperature < min_temp) {
				min_temp = json_sensor_data.sensors[i].board_temperature;
			}
		}

		// Add each point to the data array and set the weight based on the normalized temperature value
		// The data array looks like this, one for each sensor:
		// [lat, lng, normalizedTemperature]
		// [lat, lng, normalizedTemperature]
		// ...
		point_data.length = 0;
		for (i in json_sensor_data.sensors) {
			point_data.push(
				[json_sensor_data.sensors[i].lat, json_sensor_data.sensors[i].lng, normalize(json_sensor_data.sensors[i].board_temperature, max_temp, min_temp)]
			);
		}
		
		// Now that the data has been updated, redraw the heatmap
		updateHeatmap();
		console.log(point_data);
	});
}

/**************************************************************************
 Map related stuff
**************************************************************************/

var map = L.map('mapid').setView([47.876669, -114.032605], 18);

var tiles = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
	attribution: '© <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
}).addTo(map);

var idw_layer = null;
var idw_opacity = 0.3;
var idw_cell_size = 8;
var idw_exp = 3;

function updateHeatmap() {
	if (idw_layer)
		idw_layer.remove();

	idw_layer = L.idwLayer(point_data, {
		opacity: idw_opacity,
		cellSize: idw_cell_size,
		exp: idw_exp,
		max: 1,
		gradient: {
			0.3: 'blue',
			0.5: 'lime',
			0.7: 'red'
		}
	}).addTo(map);
}

/**************************************************************************
 Input spinner callbacks which modify the heatmap settings
**************************************************************************/

var $opacityInput = $("#opacity-input")
$opacityInput.on("input", function (event) {
	idw_opacity = $opacityInput.val();
	updateHeatmap();
})

var $expInput = $("#exp-input")
$expInput.on("input", function (event) {
	idw_exp = $expInput.val();
	updateHeatmap();
})